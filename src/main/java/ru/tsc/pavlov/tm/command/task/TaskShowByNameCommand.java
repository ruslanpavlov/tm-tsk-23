package ru.tsc.pavlov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.pavlov.tm.model.Task;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class TaskShowByNameCommand extends AbstractTaskCommand {

    @Override
    public UserRole[] roles() {
        return UserRole.values();
    }

    @NotNull
    @Override
    public String getName() {
        return TerminalConst.TASK_SHOW_BY_NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show tasks by name";
    }

    @Override
    public void execute() {
        @Nullable final String userId = getAuthService().getCurrentUserId();
        System.out.println("[ENTER NAME]");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final Task task = getTaskService().findByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}
