package ru.tsc.pavlov.tm.command.mutually;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.pavlov.tm.model.Task;
import ru.tsc.pavlov.tm.util.TerminalUtil;

import java.util.List;

public class TaskListByProjectIdCommand extends AbstractMutuallyCommand {

    @Nullable
    @Override
    public UserRole[] roles() {
        return UserRole.values();
    }

    @NotNull
    @Override
    public String getName() {
        return TerminalConst.TASK_LIST_BY_PROJECT_ID;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display list of tasks by project id";
    }

    @Override
    public void execute() {
        @Nullable final String userId = getAuthService().getCurrentUserId();
        System.out.println("[ENTER PROJECT ID]");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final List<Task> tasks = getProjectTaskService().findTaskByProjectId(userId, projectId);
        if (tasks.size() <= 0) throw new TaskNotFoundException(projectId);
        for (Task task : tasks)
            System.out.println(task);
    }

}
