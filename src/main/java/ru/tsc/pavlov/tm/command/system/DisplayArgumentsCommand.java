package ru.tsc.pavlov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.command.AbstractCommand;
import ru.tsc.pavlov.tm.constant.TerminalConst;


import java.util.Collection;

public class DisplayArgumentsCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return TerminalConst.ARGUMENTS;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display list of arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Collection<AbstractCommand> arguments = serviceLocator.getCommandService().getArguments();
        for (final AbstractCommand argument : arguments) {
            System.out.println(argument.getArgument());

        }
    }

}