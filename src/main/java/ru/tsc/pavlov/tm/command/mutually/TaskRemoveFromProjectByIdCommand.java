package ru.tsc.pavlov.tm.command.mutually;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.exception.entity.MutualNotFoundException;
import ru.tsc.pavlov.tm.model.Task;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class TaskRemoveFromProjectByIdCommand extends AbstractMutuallyCommand {

    @Nullable
    @Override
    public UserRole[] roles() {
        return UserRole.values();
    }

    @NotNull
    @Override
    public String getName() {
        return TerminalConst.TASK_REMOVE_FROM_PROJECT_BY_ID;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove task from project by id";
    }

    @Override
    public void execute() {
        @Nullable final String userId = getAuthService().getCurrentUserId();
        System.out.println("[ENTER PROJECT ID]");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID]");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @Nullable final Task taskUpdated = getProjectTaskService().unbindTaskById(userId, projectId, taskId);
        if (taskUpdated == null) throw new MutualNotFoundException(taskId, projectId);
        System.out.println("[OK]");
    }

}
