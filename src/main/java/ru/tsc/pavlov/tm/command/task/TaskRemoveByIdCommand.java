package ru.tsc.pavlov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @Override
    public UserRole[] roles() {
        return UserRole.values();
    }

    @NotNull
    @Override
    public String getName() {
        return TerminalConst.TASK_REMOVE_BY_ID;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove task by id";
    }

    @Override
    public void execute() {
        System.out.println("[ENTER ID]");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final String userId = getAuthService().getCurrentUserId();
        if (!getTaskService().existsById(id)) throw new TaskNotFoundException();
        getTaskService().removeById(userId, id);
        System.out.println("[OK]");
    }

}
