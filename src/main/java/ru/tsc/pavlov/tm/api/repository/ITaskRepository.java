package ru.tsc.pavlov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.enumerated.Status;
import ru.tsc.pavlov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

    boolean existsByIndex(@NotNull String userId, @NotNull int index);

    boolean existsByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task findByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task findByIndex(@NotNull String userId, @NotNull int index);

    @Nullable
    Task removeByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task removeByIndex(@NotNull String userId, @NotNull int index);

    @Nullable
    Task startById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task startByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task startByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task finishById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task finishByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task finishByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @Nullable
    Task changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    @Nullable
    Task changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status);

    @Nullable
    Task bindTaskToProjectById(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    @Nullable
    Task unbindTaskById(@NotNull String userId, @NotNull String taskId);

    void removeAllTaskByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    List<Task> findAllTaskByProjectId(@NotNull String userId, @NotNull String projectId);

}
