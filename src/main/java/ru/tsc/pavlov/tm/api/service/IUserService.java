package ru.tsc.pavlov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.api.IService;
import ru.tsc.pavlov.tm.model.User;
import ru.tsc.pavlov.tm.enumerated.UserRole;

import java.util.List;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable UserRole role);

    void setRole(@Nullable String id, @Nullable UserRole role);

    @NotNull
    User findByLogin(@Nullable String login);

    @NotNull
    User findByEmail(@Nullable String email);

    void setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    User removeByLogin(@NotNull String login);

    void updateByLogin(@Nullable String login, @Nullable String lastName, @Nullable String firstName, @Nullable String middleName, @Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
