package ru.tsc.pavlov.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.api.repository.*;
import ru.tsc.pavlov.tm.api.service.*;
import ru.tsc.pavlov.tm.command.AbstractCommand;
import ru.tsc.pavlov.tm.command.auth.AuthLoginCommand;
import ru.tsc.pavlov.tm.command.auth.AuthLogoutCommand;
import ru.tsc.pavlov.tm.command.mutually.*;
import ru.tsc.pavlov.tm.command.project.*;
import ru.tsc.pavlov.tm.command.system.*;
import ru.tsc.pavlov.tm.command.task.*;
import ru.tsc.pavlov.tm.command.user.*;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.exception.empty.EmptyCommandException;
import ru.tsc.pavlov.tm.exception.system.UnknownCommandException;
import ru.tsc.pavlov.tm.repository.*;
import ru.tsc.pavlov.tm.service.*;
import ru.tsc.pavlov.tm.util.StringUtil;
import ru.tsc.pavlov.tm.util.TerminalUtil;

@Getter
public class Bootstrap implements ServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthRepository authRepository = new AuthRepository();

    @NotNull
    private final IAuthService authService = new AuthService(authRepository, userService);

    {
        execute(new AboutApplication());
        execute(new AboutVersion());
        execute(new HelpCommand());
        execute(new InfoCommand());
        execute(new ExitCommand());
        execute(new DisplayArgumentsCommand());
        execute(new DisplayCommands());

        execute(new ProjectChangeStatusByIdCommand());
        execute(new ProjectRemoveByIdCommand());
        execute(new ProjectChangeStatusByIndexCommand());
        execute(new ProjectStartByNameCommand());
        execute(new ProjectChangeStatusByNameCommand());
        execute(new ProjectClearCommand());
        execute(new ProjectCreateCommand());
        execute(new ProjectFinishByIdCommand());
        execute(new ProjectFinishByIndexCommand());
        execute(new ProjectFinishByNameCommand());
        execute(new ProjectListCommand());
        execute(new ProjectShowByIdCommand());
        execute(new ProjectShowByIndexCommand());
        execute(new ProjectShowByNameCommand());
        execute(new ProjectStartByIdCommand());
        execute(new ProjectStartByIndexCommand());
        execute(new ProjectChangeStatusByNameCommand());
        execute(new ProjectUpdateByIdCommand());
        execute(new ProjectUpdateByIndexCommand());

        execute(new TaskChangeStatusByIdCommand());
        execute(new TaskChangeStatusByIndexCommand());
        execute(new TaskChangeStatusByNameCommand());
        execute(new TaskClearCommand());
        execute(new TaskCreateCommand());
        execute(new TaskFinishByIdCommand());
        execute(new TaskFinishByIndexCommand());
        execute(new TaskFinishByNameCommand());
        execute(new TaskListCommand());
        execute(new TaskRemoveByIdCommand());
        execute(new TaskRemoveByIndexCommand());
        execute(new TaskRemoveByNameCommand());
        execute(new TaskShowByIdCommand());
        execute(new TaskShowByIndexCommand());
        execute(new TaskShowByNameCommand());
        execute(new TaskStartByIdCommand());
        execute(new TaskStartByIndexCommand());
        execute(new TaskStartByNameCommand());
        execute(new TaskUpdateByIdCommand());
        execute(new TaskUpdateByIndexCommand());

        execute(new ProjectRemoveByIdCommand());
        execute(new ProjectRemoveByIndexCommand());
        execute(new ProjectRemoveByNameCommand());
        execute(new TaskAddToProjectByIdCommand());
        execute(new TaskListByProjectIdCommand());
        execute(new TaskRemoveFromProjectByIdCommand());

        execute(new UserChangePasswordCommand());
        execute(new UserCreateCommand());
        execute(new UserRemoveByIdCommand());
        execute(new UserRemoveByLoginCommand());
        execute(new UserShowCommand());
        execute(new UserUpdateByLoginCommand());
        execute(new UserLockByLoginCommand());
        execute(new UserUnlockByLoginCommand());

        execute(new AuthLoginCommand());
        execute(new AuthLogoutCommand());
    }

    public void start(@Nullable final String[] args) {
        logService.debug("Test environment");
        System.out.println("** WELCOME TO TASK MANAGER **");
        parseArgs(args);
        initData();
        while (true) {
            try {
                System.out.println("ENTER COMMAND");
                @NotNull final String command = TerminalUtil.nextLine();
                logService.command(command);
                parseCommand(command);
                logService.info("Command complete.");
            } catch (@NotNull final Exception e) {
                logService.error(e);
            }
        }

    }

    private void initUser(){
        userService.create("ADMIN","Qwerty","admin@yandex.ru");
        userService.create("USER","123456","user@mail.ru");
    }

    @NotNull String adminId = userService.create("ADMIN","Qwerty",UserRole.ADMIN).getId();
    @NotNull String userId = userService.create("USER","123456",UserRole.USER).getId();

    private void initData() {
        projectService.create(userId,"Project №6", "number 7");
        projectService.create(userId,"Project №5", "number 5");
        projectService.create(adminId,"Project №3", "number 3");
        projectService.create(adminId,"Project №2", "number 2");
        projectService.create(adminId,"Project №1", "number 1");
        projectService.create(adminId,"Project №4", "number 4");
    }

    public void parseCommand(@Nullable final String command) {
        if (StringUtil.isEmpty(command)) throw new EmptyCommandException();
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException(command);
        final UserRole[] roles = abstractCommand.roles();
        authService.checkRoles(roles);

        abstractCommand.execute();
    }

    public void execute(@NotNull final AbstractCommand command) {
        if (command == null)
            return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void parseArg(@Nullable final String arg) {
        final AbstractCommand command = commandService.getCommandByArg(arg);
        command.execute();
    }

    public boolean parseArgs(String[] args) {
        if (StringUtil.isEmpty(args)) return false;
        else {
            final String arg = args[0];
            parseArg(arg);
            return true;
        }
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public IUserService getUserService() { return userService; }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}
